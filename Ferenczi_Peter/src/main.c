/**

\brief Entry point of hello app.
\version 1.0
\author ferenczipeter
*/

#include <stdlib.h>
#include <stdio.h>

#include "../include/hello.h"


int main (int argc, char ** argv) {
    if (hel_init() != EXIT_SUCCESS){
	printf("bukta\n");
	return EXIT_FAILURE;
    }
    hel_say();
    hel_deinit();
    return EXIT_SUCCESS;
}
