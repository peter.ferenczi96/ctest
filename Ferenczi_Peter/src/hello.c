/**

*/

#include <stdio.h>
#include <stdlib.h>

#include "../include/hello.h"

#ifdef debug
#define HEL_DEBUG
#else
#undef HEL_DEBUG
#endif


int hel_init () {
#ifdef HEL_DEBUG
    printf("Enter hel_init\n");
#endif
return EXIT_SUCCESS;
}

void hel_deinit () 
{
#ifdef HEL_DEBUG
    printf("Enter hel_deinit\n");
#endif
}

void hel_say () {
    printf("Helloworld\n");
}
